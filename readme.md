# Evaluación Práctica III, Semana 12
#### DESCRIPCIÓN:
En el siguiente proyecto se muestra un registro de personas(CRUD) almacenando los datos ingresados y guardandolos para su futura utilidad.
Creado con el lenguaje de programación JAVA ambiente web en el IDE de NetBeans

### INTEGRANTES TEAM DEVELOPMENT #3
| Alumno  | Rol  | Tareas  |
| ------------ | ------------ | ------------ |
|David Enrique Flores Valladares| Scrum Master  |Desarrollo del método para localizar datos y verificación de errores   |
|Abel Ernesto López Hernández   |Backend   |Desarrollo e implementación del método "Eliminar"   |
|Alexis Giovanni Angel López    | Backend   |Desarrollo e implementación del método "Actualizar"   |
|Edwin Alexander Hernández Grande  | Frontend   |Diseño de vistas   |
|Samuel Antonio Hernández Chorro   |Frontend   |Diseño de vistas   |

### DOCENTE:
<ul>
    <li>Manuel de Jesús Gámez López</li>
</ul>