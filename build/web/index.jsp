<%-- 
    Document   : index
    Created on : 06-05-2021, 01:40:34 PM
    Author     : Josam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>INICIO</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/estilos.css" media="screen" />
    </head>
    <body>
        
        <div class="container py-4 mx-auto">

            <div class="row align-items-md-stretch">
              <div class="col-md-6">
                <div class="h-100 p-5 text-light bg-primary border border-5 border-dark rounded-3 ">
                    <h2 class="tt1">Registrar nueva persona</h2>
                  <p></p>
                  <a href="registrar.jsp" class="btn btn-dark box1 rounded-pill border-dark">Registrar</a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="h-100 p-5 bg-warning border-5 border-dark rounded-3">
                    <h2 class="tt2">Ver todos los registros</h2>
                  <p></p>
                    <form action="mostrar.do">
                        <input type="submit" class="btn btn-danger box2 rounded-pill border-dark" value="Ver registros"/>
                    </form>
                </div>
              </div>
            </div>
        </div>
    
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</html>