<%-- 
    Document   : index
    Created on : 06-05-2021, 08:57:07 AM
    Author     : Sam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/estilos.css" media="screen" />
    </head>
    <body>
        <div class="caja-registro bg-dark mx-auto shadow p-3 mb-5 rounded-3 border-5">
            <h1 class="fs-1 text-light position-relative" >Registro de personas</h1>
        
        <form action="recibir.do" method="POST">
            
            <label class="dui form-label"><strong> DUI:</strong></label><input type="text" name="txtDui" value="" class="input-dui form-control"/><br><br>
            <label class="apellidos form-label"><strong>Apellidos:</strong></label><input type="text" name="txtApellidos" value="" class="form-control input-apellidos"/><br><br>
            <label class="nombres form-label"><strong>Nombres:</strong></label><input type="text" name="txtNombres" value="" class="input-nombres form-control"/><br><br>
            <center>
                <input type="submit" value="Registrar Nueva Persona" class="btn btn-success"/><br><br>
                <a href="index.jsp" class="btn btn-warning"/>Menú Principal</a>
            </center>
        
        </form>
        </div>
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</html>