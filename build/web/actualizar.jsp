
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : actualizar
    Created on : 06-05-2021, 04:25:33 PM
    Author     : Josam
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/estilos.css" media="screen" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Actualizar</title>
     
    </head>
    <body>
       
        <div class="container-actualizar bg-dark mx-auto shadow p-3 mb-5 rounded-3 border-5">
             <h1 align="center"class="text-light" >Actualizar Registro!</h1>
                    <form action="actualizar.do" method="POST">
                        <c:forEach var="listaTotal" items="${sessionScope.personas}">
                            <label class="dui form-label"><strong> DUI:</strong></label>
                            <input type="text" name="txtDui" value="${listaTotal.dui}" readonly class="input-dui form-control"/><br><br>

                            <label class="apellidos form-label"><strong>Apellidos:</strong></label>
                            <input type="text" name="txtApellidos" value="${listaTotal.apellidos}" class="input-apellidos form-control"/><br><br>

                            <label class="nombres form-label"><strong>Nombres:</strong></label>
                            <input type="text" name="txtNombres" value="${listaTotal.nombres}" class="input-nombres form-control"/><br><br>
                        </c:forEach>
                        <input type="submit" value="Actualizar Registro" class="btn btn-success border-2 border-dark form-control"/>
                    </form>
                    <form action="mostrar.do">
                        <input type="submit" class="btn btn-info border-2 border-dark form-control" value="Regresar"/>
                    </form>
  
        </div>
        
    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

</html>
